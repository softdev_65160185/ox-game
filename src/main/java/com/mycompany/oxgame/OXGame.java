/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.oxgame;


import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class OXGame {
    public static void main(String[] args) {
        System.out.println("OX Game");
        char[][] board = new char[3][3];
        initializeBoard(board);
        
        char player = 'X';
        boolean gameOver = false;
        Scanner kb = new Scanner(System.in);
        int n=0;
        while(!gameOver){
            n++;
            if(n==10){
            System.out.println("Draw");
            String g = playAgain(kb);
            if (g.equals("y")) {
                    resetBoard(board);
                    initializeBoard(board);
                    n = 0;
                    continue;
                } else {
                    break;
                }       
            }
            printBoard(board);
            System.out.print("Player" + player + " enter: ");
            int row = kb.nextInt();
            int col = kb.nextInt();
            System.out.println();
            
            if (board[row][col]=='-'){
                board[row][col] = player;
                if(haveWon(board, player)){
                    System.out.println("Player"+player+" Won");
                    printBoard(board);
                    String g = playAgain(kb);
                    if (g.equals("y")) {
                        resetBoard(board);
                        initializeBoard(board);
                        n = 0;
                    } else {
                        break;
                    }
                }else{
                    if(player == 'X'){
                      player = '0';
                    }else{
                      player = 'X';
                    }    
                }
            }else{
                System.out.println("Error! Try again.");
            }
        }
    }
    public static boolean haveWon(char[][] board,char player){
        for(int row = 0;row<3;row++){
            if(board[row][0] == player && board[row][1]== player && board[row][2]== player){
                return true;
            }
        }
        for(int col = 0; col<3;col++){
            if(board[0][col] == player && board[1][col]== player && board[2][col]== player){
                    return true;
            }
        }
        if(board[0][0]==player && board[1][1] == player && board[2][2] == player){
            return true;
        }
        if(board[0][2]==player && board[1][1] == player && board[2][0] == player){
            return true;
        }
        return false;
        
    }
    
    public static void printBoard(char[][] board){
        for(int row = 0;row<3;row++){
            for(int col = 0; col<3;col++){
                System.out.print(board[row][col]+" ");
            }
            System.out.println();
        }
    }
    
    public static void initializeBoard(char[][] board) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }
    
    public static String playAgain(Scanner kb) {
        System.out.print("Do you want to play again? (y/n): ");
        String g = kb.next();
        return g;
    }
    
    public static void resetBoard(char[][] board) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }
}

